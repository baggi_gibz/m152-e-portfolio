/**
 * jQuery Plugin fileContentReader
 *
 * @dependencies:
 *
 */
(function($, window, document){
  if (!$.baggi) {
    $.baggi = {}
  }

  $.baggi.fileContentReader = function(options) {
    var init, plugin, defaults, settings, refresh

    plugin = this

    defaults = {
      selector : ".js-fileContentReader",
      fileNameSelector: ".file"
    }

    init = function(){
      settings = $.extend({}, defaults, options)
      $(settings.selector).each(function(i, e){
        if($(e).find(settings.fileNameSelector).length){
          var filename = $(e).find(settings.fileNameSelector).html()
          $.ajax({
            url: filename,
            dataType: "text", // because local and school server behave different
            success: function(data) {
              $(e).text(data)
              $(e).before("<span>" + filename.split("/").pop() + "</span>")
              sh_highlightDocument()
            },
            error: function(){
              console.log("an error occured while fetching file '" + filename + "'")
            }
          })
        }
      })
    }

    plugin.refresh = function(){init()}

    init()
  }
}(jQuery, window, document));
