//

(function($){
  if (!$.baggi) {
    $.baggi = {}
  }
  $.baggi.init = function() {
    $.baggi.loadedPlugins = ["treeview"]
    $.baggi.afterRenderDo = [
      function(){
        $.baggi.videoController = new $.baggi.videoController()
        $.baggi.loadedPlugins.push("videoController")
      }]
    if ($(".js-treeview").length) {
      $.baggi.treeview = new $.baggi.treeview(".js-treeview");
    }
  }
}($, window));

$(document).ready(function() {
    $.baggi.init()
});
