/**
 * Treeview JavaScripts
 * jQuery Plugin treeview
 *
 * @dependencies:
 *
 */
(function($, window, document){
  if (!$.baggi) {
    $.baggi = {}
  }

  $.baggi.treeview = function(el, options) {
    var init, plugin, defaults, settings, handleCategory, handleSite, handleSelect, renderContent, currentSite, refreshPlugins, loadNewPlugin, postRender

    plugin = this

    defaults = {
      dataUrl : "data/",
      content: "#content",
      pluginUrl : "js/"
    }

    init = function(){
      settings = $.extend({}, defaults, options)
      currentSite = ""
      $.getJSON( "data/sites.json", function( data ) {
        $.each(data.categories, function(i,v){handleCategory(v)})
        $.each(data.sites, function(i,v){handleSite(v)})
        $(el).menu({
          items: "> :not(.ui-widget-header)",
          select: function(event, ui) {handleSelect(event, ui)}
        })
        if (window.location.hash !== "") {
          handleSelect({},{item : $(window.location.hash)})
        } else {
          postRender()
        }
      });
    }

    handleCategory = function(category) {
      if ('disabled' in category) {
        return
      }
      $('<li class="ui-widget-header" id="' + category.name + '"><div>' + category.title + '</div></li>').appendTo(el)
    }

    handleSite = function(site){
      if ('disabled' in site) {
        return
      }
      if ('plugin' in site) {
        var newSite = $('<li id="' + site.name + '"></li>')
        var container = $('<div>' + site.title + '</div>')
        $.each(site.plugin, function(i, e){
          container.append('<span class="plugin" style="display: none;">' + e + '</span>')
        })
        container.append('<span class="href" style="display: none;">' + site.content + '</span>')
        newSite.append(container)
        newSite.insertAfter("#" + site.category)
        //$('<li id="' + site.name + '"><div><span class="plugin" style="display: none;">' + site.plugin + '</span><span class="href" style="display: none;">' + site.content + '</span>' + site.title + '</div></li>').insertAfter("#" + site.category)
      } else {
        $('<li id="' + site.name + '"><div><span class="href" style="display: none;">' + site.content + '</span>' + site.title + '</div></li>').insertAfter("#" + site.category)
      }
    }

    handleSelect = function(event, ui){
      var url = settings.dataUrl + $(ui.item).find(".href").html()
      var newPage = false
      var ajaxWaitingFor

      $.baggi.afterRenderDo.push(function(){
        refreshPlugins()
      })

      if (!$(settings.content).length) { // First call -> initialise
        $('<div id="'+settings.content.substr(1)+'"></div>').appendTo("body")
        $('<span id="loadedContent" style="display: none;"></span>').insertAfter(settings.content)
        newPage = true
      }
      if ($(ui.item).attr('id') === currentSite) { // Same as already loaded -> ignore
        return;
      } else if ($("#loadedContent").find("#site-" + $(ui.item).attr('id')).length) { // Site already loaded once -> reload
        if (!newPage && !$("#loadedContent").find("#site-"+currentSite).length) {
          $("#loadedContent").append('<span id="site-'+currentSite+'">'+$(settings.content).html()+'</span>')
        }
        $(settings.content).html($("#site-" + $(ui.item).attr('id')).html())
        window.location.hash = $(ui.item).attr('id')
        postRender()
      } else { // New Site -> get and save current page
        if (!newPage && !$("#loadedContent").find("#site-"+currentSite).length) {
          $("#loadedContent").append('<span id="site-'+currentSite+'">'+$(settings.content).html()+'</span>')
        }
        $(settings.content).html("")
        ajaxWaitingFor = $.ajax({
          method: "GET",
          url: url,
          success: function(data){renderContent(data)}
        }).done(function(){})
        window.location.hash = $(ui.item).attr('id')

        if ($(ui.item).find("span.plugin").length) {
          $(ui.item).find("span.plugin").each(function(i, e){
            if (!$.baggi.loadedPlugins.includes($(e).html())) {
              loadNewPlugin($(e).html())
            }
          })
        }
      }

      currentSite = $(ui.item).attr('id')
    }

    renderContent = function(data) {
      $(settings.content).html(data)
      sh_highlightDocument()
      postRender()
    }

    refreshPlugins = function(only) {
      if (typeof only === 'undefined') {
        only = [...$.baggi.loadedPlugins]
      }
      $.each(only, function(i, v){
        if ($.baggi[v] && $.baggi[v].refresh && typeof $.baggi[v].refresh === 'function') {
          console.log("'" + v + "' refreshed")
          $.baggi[v].refresh()
        }
      })
    }

    loadNewPlugin = function(p){
      $.getScript(settings.pluginUrl + p + ".js", function(){
        $.baggi.loadedPlugins.push(p)
        console.log("Script '" + p + "' was loaded successfully")
        if (typeof $.baggi[p] === 'function') {
          $.baggi[p] = new $.baggi[p]();
        }
      })
    }

    postRender = function(){
      $.each($.baggi.afterRenderDo, function(i, e){e()})
      $.baggi.afterRenderDo = []
    }

    plugin.debug = refreshPlugins

    init()
  }
}(jQuery, window, document));
