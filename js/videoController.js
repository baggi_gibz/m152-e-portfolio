/**
 * jQuery Plugin videoContoller
 *
 * @dependencies:
 *
 */
(function($, window, document){
  if (!$.baggi) {
    $.baggi = {}
  }

  $.baggi.videoController = function(options) {
    var init, plugin, defaults, settings, refresh, refreshSingle, changePos, toggle, changeSpeed, setPos, refreshScrollbar, datas, handleTracks

    plugin = this

    defaults = {
      selector : "video",
      backwards: ".backwards",
      forwards : ".forwards",
      toggle : ".toggle",
      playspeed : ".speed",
      scroller : ".scroller"
    }

    datas = new Map()

    init = function(){
      settings = $.extend({}, defaults, options)
      $("#content").find(settings.selector).each(function(i, e){
        if (!$(e).parent(".js-videoController").length) {
          $(e).wrap("<div class=\"js-videoController\"></div>")
          $(e).after('<div class="scroller"></div><br> <button class="backwards">5 Sekunden zurück</button> <button class="toggle">Start</button> <button class="forwards">5 Sekunden Vorwärts</button> <br> <input type="range" min="0.1" max="5" step="0.1" class="speed" value="1">')
          if ($(e).get(0).textTracks.length == 1) {
            $(e).get(0).textTracks[0].mode = "showing"
          } else if ($(e).get(0).textTracks.length > 1) {
            var tracks = $("<select class=\"tracks\"></select>")
            $.each($(e).get(0).textTracks, function(i, e){
              if (i == 0) {
                e.mode = "showing"
                tracks.append("<option selected>" + e.language + "</option>")
              } else{
                e.mode = "disabled"
                tracks.append("<option>" + e.language + "</option>")
              }
            })
            $(e).get(0).textTracks[0].mode = "showing"
            tracks.click(function(){handleTrack(e)})
            tracks.insertAfter($(e).parent().find(settings.forwards))
          }
        }
        plugin.refreshSingle(e)
      })
    }

    changePos = function(e, pos) {
      $(e).get(0).currentTime += pos
    }

    toggle = function(e){
      if(datas.get($(e).get(0)).isPlaying){
        $(e).parent().find(settings.toggle).html("Start")
        datas.get($(e).get(0)).isPlaying = false
        $(e).get(0).pause()
      } else {
        $(e).parent().find(settings.toggle).html("Stop")
        datas.get($(e).get(0)).isPlaying = true
        $(e).get(0).play()
      }
    }

    changeSpeed = function(e){
      $(e).get(0).playbackRate = $(e).parent().find(settings.playspeed).val()
    }

    setPos = function(e) {
      $(e).get(0).currentTime = ($(e).parent().find(settings.scroller).slider("option", "value")/1000)*$(e).get(0).duration
    }

    refreshScrollbar = function(e){
      if ($(e).is(":hidden") ) {
        return
      }
      if (datas.get($(e).get(0)).isPlaying && datas.get($(e).get(0)).sliderNotDragged) {
        $(e).parent().find(settings.scroller).slider("option", "value", ($(e).get(0).currentTime/$(e).get(0).duration)*1000)
      }
    }

    handleTrack = function(e){
      var lang = $(e).parent().find(".tracks").val()
      $.each($(e).get(0).textTracks, function(i, e){
        if (e.mode === "showing" && e.language!==lang) {
          e.mode = "disabled"
        } else if (e.language===lang) {
          e.mode = "showing"
        }
      })
    }

    plugin.refresh = function(){
      init()
      $("#content").find(settings.selector).each(function(){
        plugin.refreshSingle(this)
      })
    }

    plugin.refreshSingle = function(e) {
      if (datas.has($(e).get(0))) {
        return
      }
      var data = {
        isPlaying: false,
        sliderNotDragged: true
      }
      datas.set($(e).get(0), data)
      $(e).parent().find(settings.forwards).click(function(){changePos(e, 5)})
      $(e).parent().find(settings.backwards).click(function(){changePos(e, -5)})
      $(e).parent().find(settings.toggle).click(function(){toggle(e)})
      $(e).parent().find(settings.playspeed).click(function(){changeSpeed(e)})
      $(e).on('ended', function(){toggle(e)})

      $(e).parent().find(settings.scroller).slider({
        min:0,
        max:1000,
        slide:function(){datas.get($(e).get(0)).sliderNotDragged=false},
        stop:function(){
          datas.get($(e).get(0)).sliderNotDragged=true
          setPos(e)
        }
      })
      window.setInterval(function(){refreshScrollbar(e)})
    }

    init()
  }
}(jQuery, window, document));
