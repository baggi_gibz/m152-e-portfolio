/**
 * jQuery Plugin streamingController
 *
 * @dependencies:
 *
 */
(function($, window, document){
  if (!$.baggi) {
    $.baggi = {}
  }

  $.baggi.streamingController = function(options) {
    var init, plugin, defaults, settings, refresh, streams, initializedVideos

    plugin = this

    initializedVideos = []

    defaults = {
      selector : ".js-streamingController",
      streamBase : "streams/"
    }

    init = function(){
      settings = $.extend({}, defaults, options)
      $.getJSON(settings.streamBase + "streams.json", function( data ) {
        streams = data.streams
      }).done(plugin.refresh)
    }

    plugin.refresh = function(){
      $(settings.selector).each(function(i, e){
        $.each(streams, function(j, f){
          if ($(e).get(0).id === f.name && $.inArray(f.name, initializedVideos) === -1) {
            //initializedVideos.push(f.name)
            var player = new shaka.Player($(e).get(0))
            player.load(settings.streamBase + f.manifest).then(function() {
              console.log('The video has now been loaded!')
            })
          }
        })
      })
      $.baggi.videoController.refresh()
    }

    init()
  }
}(jQuery, window, document));
